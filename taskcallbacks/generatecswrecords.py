"""Generate metadata records for CGLOPS public products

This module holds functions to perform the generation of XML metadata records
for CGLOPS products and uploading them to the relevant CSW catalogues.

"""

import logging
from os import getenv
import re

import requests
from lxml import etree

from netcdf_cglops_tools import netcdfmetadata

logger = logging.getLogger(__name__)

NAMESPACES = {
    'csw': 'http://www.opengis.net/cat/csw/2.0.2',
    'ogc': 'http://www.opengis.net/ogc',
    'dc': 'http://purl.org/dc/elements/1.1/',
    'ows': 'http://www.opengis.net/ows',
    'gco': 'http://www.isotc211.org/2005/gco',
    'gmd': 'http://www.isotc211.org/2005/gmd',
}

REQUEST_HEADERS = {
    'Content-Type': 'application/xml',
    'Accept': 'application/xml',
}


def generate_csw_record(retrieved_product, *args, **kwargs):
    """Generate XML record for the product and update metadata catalogues.

    Parameters
    ----------
    retrieved_product: soko.packages.RetrievedProduct
        Information about the retrieval to the product. This includes the
        product's local path, its original URL and the package member that
        originated it.

    """

    logger.debug(
        "Generating metadata record from "
        "{!r}...".format(retrieved_product.local_path)
    )
    metadata_record = generate_record(retrieved_product.local_path)
    record_id = get_record_id(metadata_record)
    logger.debug("record_id: {}".format(record_id))
    errors = 0
    csw_catalogues = getenv("METADATA_CATALOGUES", "").split()
    logger.debug("csw_catalogues: {}".format(csw_catalogues))
    if not any(csw_catalogues):
        raise RuntimeError(
            "No metadata catalogues are currently configured")
    for catalogue in csw_catalogues:
        logger.debug(
            "Looking for already existing record in {!r}...".format(catalogue))
        try:
            if record_exists(record_id, catalogue):
                logger.debug("Found existing record. Deleting before "
                             "inserting the current one...")
                delete_record(record_id, catalogue)
            logger.debug("Inserting record into {!r}...".format(catalogue))
            insert_record(metadata_record, catalogue)
        except Exception:
            errors += 1
            logger.exception(
                "Could not complete interaction with {!r}".format(catalogue))
    return True if errors == 0 else False


def generate_record(product_path):
    """Generate an XML record with ISO19115 metadata

    Parameters
    ----------
    product_path: str
        Path to the CGLOPS product for which the metadata record is to be
        generated

    Returns
    -------
    str
        The generated XML metadata record

    """

    metadata_generator = netcdfmetadata.NetCdfMetadataGenerator()
    metadata = metadata_generator.generate_metadata(product_path)
    encoded = metadata.encode("utf-8")
    return encoded


def get_record_id(metadata):
    """Extract record identifier from the metadata."""
    element = etree.fromstring(metadata)
    return element.xpath(
        "gmd:fileIdentifier/gco:CharacterString/text()",
        namespaces=element.nsmap
    )[0]


def get_record_by_id(record_id, catalogue_url, element_set_name="brief",
                     output_schema="http://www.opengis.net/cat/csw/2.0.2"):
    """Get a single record from a CSW catalogue.

     This function performs the CSW GetRecordById operation.

    Parameters
    ----------
    record_id: str
        Record identifier to use in the GetRecordById operation
    catalogue_url: str
        URL of the CSW catalogue to interact with
    element_set_name: str, optional
        Value for the ``ElementSetName`` parameter of the GetRecordById
        request. Defaults to _brief_. Valid values are _brief_, _summary_ and
        _full_.
    output_schema: str, optional
        Value for the ``OutputSchema`` parameter of the GetRecordById request.
        Defaults to the CSW v2.0.2 output schema, meaning that the retrieved
        record will use the ``csw:Record`` schema

    Returns
    -------
    str
        The retrieved metadata record

    """

    output_format = "application/xml"
    response = requests.get(
        catalogue_url,
        params={
            "service": "CSW",
            "version": "2.0.2",
            "request": "GetRecordById",
            "ElementSetName": element_set_name,
            "outputFormat": output_format,
            "outputSchema": output_schema,
            "Id": record_id
        },
        headers={
            "Accept": output_format,
        }
    )
    response.raise_for_status()
    response_element = etree.fromstring(str(response.text))
    return response_element


def delete_record(record_id, catalogue_url):
    """Delete a record from the CSW catalogue.

    This function performs the CSW Transaction operation with a Delete
    payload.

    Parameters
    ----------
    record_id: str
        Identifier of the record that is to be delete from the catalogue
    catalogue_url: str
        URL of the CSW catalogue to interact with

    """

    global NAMESPACES
    payload = (
        '<csw:Delete><csw:Constraint version="1.1.0"><ogc:Filter>'
        '<ogc:PropertyIsEqualTo>'
        '<ogc:PropertyName>apiso:Identifier</ogc:PropertyName>'
        '<ogc:Literal>{id}</ogc:Literal>'
        '</ogc:PropertyIsEqualTo></ogc:Filter></csw:Constraint></csw:Delete>'
    )
    transaction = _wrap_with_csw_transaction(payload.format(id=record_id))
    response_element = _post_request_to_catalogue(transaction, catalogue_url)
    num_deleted = int(response_element.xpath(
        "csw:TransactionSummary/csw:totalDeleted/text()",
        namespaces=NAMESPACES
    )[0])
    if num_deleted != 1:
        logger.error(response_element.text)
        raise RuntimeError("Could not delete record in "
                           "the {!r} catalogue".format(catalogue_url))
    return True


def insert_record(record, catalogue_url):
    """Insert a record into the CSW catalogue.

    This function performs the CSW Transaction operation with an Insert
    payload.

    Parameters
    ----------
    record: str
        The metadata record that is to be inserted in the CSW catalogue
    catalogue_url: str
        URL of the CSW catalogue to interact with

    Raises
    ------
    RuntimeError
        When there is an error with the insertion of the metadata
        record in the catalogue

    """

    global NAMESPACES
    payload = "<csw:Insert>{record}</csw:Insert>".format(
        record=_remove_xml_declaration(record))
    transaction = _wrap_with_csw_transaction(payload)
    response_element = _post_request_to_catalogue(transaction, catalogue_url)
    num_inserted = int(response_element.xpath(
        "csw:TransactionSummary/csw:totalInserted/text()",
        namespaces=NAMESPACES
    )[0])
    if num_inserted != 1:
        logger.error(response_element.text)
        raise RuntimeError("Could not insert record in "
                           "the {!r} catalogue".format(catalogue_url))
    return True


def record_exists(record_id, catalogue_url):
    """Test if a metadata record exists on the input CSW catalogue.

    Parameters
    ----------
    record_id: str
        Identifier of the metadata record. This is used to perform a CSW
        GetRecordById operation.
    catalogue_url: str
        URL of the CSW catalogue to interact with

    Returns
    -------
    bool
        Whether the record exists on the catalogue or not.

    """

    response = get_record_by_id(record_id, catalogue_url,
                                element_set_name="brief")
    returned_records = response.xpath(
        "csw:BriefRecord", namespaces=response.nsmap)
    return True if len(returned_records) > 0 else False


def _get_formatted_namespaces():
    """Utility function to format XML namespaces."""
    global NAMESPACES
    return " ".join(
        'xmlns:{}="{}"'.format(k, v) for k,v in NAMESPACES.items())


def _post_request_to_catalogue(request, catalogue_url):
    """Utility function to perform an HTTP POST request to a CSW catalogue."""
    global REQUEST_HEADERS
    response = requests.post(
        catalogue_url,
        data=request,
        headers=REQUEST_HEADERS
    )
    response.raise_for_status()
    response_content = response.text.encode("utf-8")
    logger.debug("response_content: {}".format(response_content))
    return etree.fromstring(response_content)


def _remove_xml_declaration(xml_fragment):
    """Remove the XML declaration from the input."""
    pattern = re.compile(r'\<\?xml version="1.0"( encoding="UTF-8")?')
    return pattern.sub("", xml_fragment)


def _wrap_with_csw_transaction(contents):
    """Utility function to wrap an XML payload in a CSW Transaction request."""
    wrapper_template = (
        '<?xml version="1.0"?>'
        '<csw:Transaction service="CSW" version="2.0.2" {namespaces}>'
        '{contents}'
        '</csw:Transaction>'
    )
    return wrapper_template.format(
        namespaces=_get_formatted_namespaces(), contents=contents)
