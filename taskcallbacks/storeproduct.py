import logging
from os import getenv

from soko.handlers import handlers

logger = logging.getLogger(__name__)


def store_product(retrieved_product, timeslot, **kwargs):
    package = kwargs["package"]
    output = package.outputs[0]
    output_urls = output.resource.get_output_search_urls(
        data_roots=getenv("DISSEMINATION_STORES", "").split(),
        timeslot=output.timeslot
    )
    for output_url in output_urls:
        logger.debug("Sending {!r} to {!r}...".format(
            retrieved_product.local_path, output_url))
        handlers.put_url(retrieved_product.local_path, output_url)

