import logging
import shutil

logger = logging.getLogger(__name__)


def retrieve_product(retrieved_product, output_dir, **kwargs):
    """Retrieve products from their temporary location

    This task looks for inputs in a known filesystem directory and moves
    them to their expected locations so that downstream tasks can find them.

    Parameters
    ----------
    retrieved_product: soko.packages.RetrievedMember
        Information about the retrieved product. This includes the
        product's local path, its original URL and the package member that
        originated it.

    output_dir: str
        Local directory where the product is to be put. This directory is
        expected to already exist

    """

    logger.debug("Inside the 'retrieve_product' callback...")
    logger.debug("retrieved_product: {}".format(retrieved_product))
    shutil.move(retrieved_product.local_path, output_dir)
    # now remove product from its remote FTP path
