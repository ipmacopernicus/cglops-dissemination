import logging
import os

import requests

logger = logging.getLogger(__name__)


def process_subscriptions(*args, **kwargs):
    package = kwargs["package"]
    product_input = package.inputs[0]
    dissemination_systems = os.getenv("DISSEMINATION_SYSTEMS", "").split()
    access_tokens = os.getenv(
        "DISSEMINATION_SYSTEMS_ACCESS_TOKENS", "").split()
    if not any(dissemination_systems):
        raise RuntimeError("No dissemination systems are currently configured")
    for index, dissemination_url in enumerate(dissemination_systems):
        try:
            token = access_tokens[index]
        except IndexError:
            raise RuntimeError(
                "Incorrectly defined access token. Check the "
                "DISSEMINATION_SYSTEMS_ACCESS_TOKENS environment variable"
            )
        response = requests.post(
            dissemination_url,
            data={
                "timeslot": product_input.timeslot,
                "collection": kwargs["collection"],
                "force_creation": True,
            },
            auth=TokenAuthorization(token)
        )
        try:
            response.raise_for_status()
        except Exception:
            logger.debug("response headers: {}".format(response.headers))
            logger.debug("response.text: {}".format(response.text))
            logger.debug("response.request.headers: {}".format(
                response.request.headers))
            logger.debug("response.request.url: {}".format(
                response.request.url))
            raise
        logger.debug("response: {}".format(response.json()))


class TokenAuthorization(requests.auth.AuthBase):
    """Custom requests authorization class for djangorestframework's TokenAuth.
    """

    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers["Authorization"] = b"Token {}".format(self.token)
        return r
