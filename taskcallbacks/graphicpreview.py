import logging
from os import getenv
import os.path

import mapscript
from pathlib2 import Path
from PIL import Image

from soko.handlers import handlers
from mapservertools import colormaps
from mapservertools import mapserver
from netcdf_cglops_tools import netcdfanalyzer

logger = logging.getLogger(__name__)


def generate_preview(retrieved_product, timeslot, output_dir, scratch_dir,
                     layer_name=None, band=None, **kwargs):
    preview = _generate_preview(
        retrieved_product.local_path,
        layer_name=str(layer_name),
        band=int(band),
        output_dir=output_dir,
        scratch_dir=scratch_dir
    )
    _send_preview_to_dissemination_store(
        store_urls=getenv("DISSEMINATION_STORES", "").split(),
        preview_path=preview,
        package_member=kwargs["package"].outputs[0]
    )


def _generate_preview(product_path, layer_name, band, output_dir, scratch_dir):
    """Generate a png image with the product's main variable"""
    processor = mapserver.MapfileProcessor(
        name="quicklook_generator",
        shape_path=os.path.dirname(product_path)
    )
    product_info = netcdfanalyzer.ProductInfo(product_path)
    try:
        color_map = colormaps.colormap.get(
            "{}_{}".format(product_info.name.lower(),
                           layer_name.lower()))
    except AttributeError:
        raise RuntimeError(
            "could not load color_map for {}".format(layer_name))

    new_layer = mapserver.MapfileLayer(
        name=layer_name,
        copernicus_product_id=product_info.identifier,
        data_path=product_info.variables[layer_name].gdal_path,
        projection="init=epsg:4326",
        classes=color_map,
        bands=[band],
        status=mapscript.MS_ON,
    )
    processor.layers[new_layer.qualified_name] = new_layer
    output_path = Path(output_dir) / Path(product_path).stem
    #data_img = processor.save_image(str(output_path))
    data_img = processor.save_image(
        str(Path(scratch_dir) / output_path.name))
    #legend_img = processor.save_legend(str(output_path))
    legend_img = processor.save_legend(
        str(Path(scratch_dir) / output_path.name))
    complete_quicklook = _complete_quicklook(
        data_img, legend_img, (255, 255, 255))
    file_type = data_img.rpartition(".")[-1]
    quicklook_path = str(output_path) + "_quicklook.{}".format(file_type)
    complete_quicklook.save(quicklook_path)
    return quicklook_path


def _complete_quicklook(quicklook, legend, background_color,
                        resize=0.5):
    """Complete the quicklook by joining the image and legend.

    Parameters
    ----------
    quicklook: str
        Full path to the quicklook image file
    legend: str
        Full path to the legend image file
    background_color: tuple
        Three element tuple with integers specifying the RGB color for the
        map's background.
    resize: float, optional
        A number specifying the resize ratio for the quicklook.
        The image is resized before joining with the legend, in order to
        preserve readability.

    """

    quick_im = Image.open(quicklook)
    xx, yy = quick_im.size
    new_x = int(xx * resize)
    new_y = int(yy * resize)
    resized = quick_im.resize((new_x, new_y))
    leg_im = Image.open(legend)
    joined = _join_images(resized, leg_im, background_color)
    padded = _pad_image(joined, 10, background_color)
    return padded


def _pad_image(image, ammount, background_color):
    """Add some padding to an image."""

    old_width, old_height = image.size
    width = old_width + ammount
    height = old_height + ammount
    padded = Image.new(image.mode, (width, height), background_color)
    padded.paste(image, (ammount, ammount))
    return padded


def _join_images(first, second, background_color):
    """Join two images side by side.

    Parameters
    ----------

    first: PIL.Image.Image
        First image to join
    second: PIL.Image.Image
        second image to join
    background_color: tuple
        Three element tuple with integers specifying the RGB color for
        the background.

    Returns
    -------
    PIL.Image.Image
        A PIL image object with the joined inputs side by side.

    """

    joined_x = first.size[0] + second.size[0]
    joined_y = max(first.size[1], second.size[1])
    joined = Image.new(first.mode, (joined_x, joined_y), background_color)
    joined.paste(first, (0, 0))
    joined.paste(second, (first.size[0], 0))
    return joined


def _send_preview_to_dissemination_store(store_urls, preview_path,
                                         package_member):
    output_urls = package_member.resource.get_output_search_urls(
        data_roots=store_urls,
        timeslot=package_member.timeslot
    )
    for output_url in output_urls:
        logger.debug(
            "Sending {!r} to {!r}...".format(preview_path, output_url))
        handlers.put_url(preview_path, output_url)


