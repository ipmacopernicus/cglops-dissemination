"""Airflow task callbacks for cleaning products that are not needed anymore."""

from future.standard_library import install_aliases
install_aliases()

import datetime as dt  # noqa: E402
import logging  # noqa: E402
import os  # noqa: E402
import re  # noqa: E402
from urllib.parse import urlparse  # noqa: E402

import dateutil.parser  # noqa: E402
from soko.handlers.ftphandler import get_ftp_connection  # noqa: E402

logger = logging.getLogger(__name__)


def clean_inputs(*args, **kwargs):
    """Remove any system input files that may be lingering in the entrypoint.

    The entrypoint of the system is the FTP URL used for retrieving raw inputs.
    Files whose temporal slot, as present in their filename, indicates that
    they are older than a specified amount of days will be deleted.

    Parameters
    ----------
    package: soko.packages.BasePackage
        The package being executed
    timeslot: datetime.datetime
        The temporal slot of the running package
    older_than_days: int
        How many days older than the current timeslot can a file be without
        being deleted

    """

    age_threshold = kwargs["timeslot"] - dt.timedelta(
        days=int(kwargs["older_than_days"]))
    logger.debug("age_threshold: {}".format(age_threshold))
    input_roots = kwargs["package"].SOKO_INPUT_ROOTS
    inputs_gateway_url = [r for r in input_roots if r.startswith("ftp")][0]
    parsed_url = urlparse(inputs_gateway_url)
    with get_ftp_connection(parsed_url.hostname, parsed_url.port or 21,
                            use_passive_mode=True) as ftp_host:
        deleted_files = []
        for item in ftp_host.listdir(ftp_host.getcwd()):
            item_path = os.path.join(ftp_host.getcwd(), item)
            item_slot = _extract_temporal_slot_from_path(item_path)
            if item_slot is not None and item_slot < age_threshold:
                logger.debug("File {!r} is older than the allowed threshold. "
                             "Deleting...".format(item_path))
                ftp_host.remove(item_path)
                deleted_files.append(item_path)
    logger.info("Deleted {} files".format(len(deleted_files)))
    for path in deleted_files:
        logger.debug("\t{}".format(path))


def clean_outputs(*retrieved_input_members, **kwargs):
    total_deleted = 0
    for retrieved in retrieved_input_members:
        path = retrieved.local_path
        if path is not None:
            logger.debug("About to remove {!r}...".format(path))
            os.remove(path)
            try:
                os.removedirs(os.path.dirname(path))
                total_deleted += 1
            except OSError:
                pass  # directory is not empty
    logger.info("Deleted {} files".format(total_deleted))


def _extract_temporal_slot_from_path(path):
    pattern = re.compile(
        r"(?P<year>\d{4})(?P<month>\d{2})(?P<day>\d{2})(?P<hour>\d{2})"
        r"(?P<minute>\d{2})"
    )
    found = re.search(pattern, path)
    try:
        slot = dateutil.parser.parse(found.group())
    except (AttributeError, ValueError) as exc:
        logger.debug("Could not extract temporal slot from "
                     "{!r}: {!r}".format(path, exc))
        slot = None
    return slot


