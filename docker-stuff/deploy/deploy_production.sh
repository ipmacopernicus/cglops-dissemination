ENVIRONMENT_FILE_PATH=production.env

COMPOSE_FILE_NAME="docker-stack-production.yml"
DOWNLOAD_BASE_URL=https://gitlab.com/ipmacopernicus/cglops-dissemination/
DOWNLOAD_BASE_URL+=raw/master

echo "Downloading docker stack file from master..."
wget --backups 0 ${DOWNLOAD_BASE_URL}/docker-stuff/${COMPOSE_FILE_NAME}

echo "Sourcing environment variables..."
set -o allexport
source ${ENVIRONMENT_FILE_PATH}
set +o allexport

echo "Logging in to docker registry (provide your credentials at the prompt)..."
docker login registry.gitlab.com

echo "Updating docker stack..."
docker stack deploy \
    --with-registry-auth \
    --compose-file ${COMPOSE_FILE_NAME} \
    cglops-dissemination

