apache-airflow[celery,crypto,password,postgres]
pathlib2
pillow
redis
git+https://gitlab.com/likeno/soko.git@v0.13.2#egg=soko
git+https://gitlab.com/ipmaSatTools/mapserver-tools.git@v0.3.0#egg=mapservertools
git+https://gitlab.com/ipmaSatTools/netcdf-cglops-tools.git@v1.2.0#egg=netcdf_cglops_tools
