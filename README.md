# cglops-dissemination

Dissemination processing lines for IPMA's Copernicus Global Land (cglops) team.

This project uses soko for organizing processes into tasks and airflow for 
managing schedules, dependencies and execution triggers


## Development

You can use the provided `docker-stack.yml` file together with
docker-compose in order to set up a reproducible dev environment.
Set up some environment variables and launch docker-compose
The id_cglops.pub and id_cglops keys must figure on the ~/.ssh/
directory of the host machine.

```
export DEV_ROOT=</path/to/your/dev/base/dirs>
export DATA_ROOT=</path/to/your/dev/base/data/dir>
export DISSEMINATION_STORE_AUTHORIZED_KEYS=$(cat ~/.ssh/id_cglops.pub)
export DISSEMINATION_SYSTEMS_ACCESS_TOKENS=<access-token1> [<access-token2>]
cd docker-stuff
cp ssh_config ~/.ssh/config && chmod 644 ~/.ssh/config
docker-compose -p dissem -f docker-stack-dev.yml up -d
```


### Testing airflow tasks with docker containers

After having set up the stack with docker-compose, run the following command::

```
docker exec -ti dissem_web1 airflow test <dag_name> <task_name> <timeslot>
```


The default log level on the development docker stack is set to `DEBUG`.


### Manually generating DAG runs

DAGs can be triggered manually via the airflow UI or with the
`airflow trigger_dag` CLI command. Keep in mind that for these commands to
produce the desired effect the **DAG must not be paused**.

```
docker exec -ti dissem_web1 airflow trigger_dag --run_id "test$(date)" --exec_date <some-execution-date> <dag-id>
```
