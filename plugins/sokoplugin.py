"""An Airflow plugin for the ``soko`` package.

This plugin implements an airflow operator for integration with soko.

"""

from airflow.plugins_manager import AirflowPlugin
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

from soko.utilities import load_settings
from soko.packages import get_package
from soko import runners


class SokoOperator(BaseOperator):

    @apply_defaults
    def __init__(self, soko_package, soko_package_context=None,
                 temporal_slot_context_key="ts",
                 soko_execution_kwargs=None, *args, **kwargs):
        super(SokoOperator, self).__init__(*args, **kwargs)
        self.temporal_slot_context_key = temporal_slot_context_key
        self.soko_package = soko_package
        self.soko_package_context = dict(
            soko_package_context) if soko_package_context is not None else {}
        self.soko_execution_kwargs = dict(
            soko_execution_kwargs) if soko_execution_kwargs is not None else {}

    def execute(self, context):
        """Execute a soko package.

        This functions glues together ``airflow`` and ``soko``.

        It is expected that the airflow configuration file has a ``soko``
        section with a ``settings_path`` key defined. This parameter is used
        for obtaining the location of the soko settings file. The loaded soko
        settings are updated with the airflow context. Additionally, the
        ``SOKO_HOME`` parameter is added to the context and its value is the
        same as the ``AIRFLOW_HOME`` value that is found on airflow's
        configuration.

        In order to make the integration between soko and airflow lighter on
        configuration, the soko settings are expected to follow certain
        conventions:

        * The name of a soko package should adhere to the form
          ``<airflow_task_id>_<airflow_product_name>``
        * The ``AIRFLOW_HOME`` variable is made accessible to the soko settings
          under ``SOKO_HOME``

        The actual code that is executed by a task if found on the
        ``process_callback`` configuration key in the soko settings file.

        """

        soko_settings = load_settings(
            context["conf"].get("soko", "settings_path"))
        soko_settings.update(context)
        soko_settings["context"]["SOKO_HOME"] = context["conf"].get(
            "core", "airflow_home")
        package = get_package(
            self.soko_package,
            settings=soko_settings,
            timeslot=context[self.temporal_slot_context_key],
            **self.soko_package_context
        )
        return runners.run_process(package, **self.soko_execution_kwargs)


class SokoPlugin(AirflowPlugin):
    name = "soko_plugin"
    operators = [
        SokoOperator,
    ]
