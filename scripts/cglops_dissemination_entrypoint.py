#!/usr/bin/env python
"""An entrypoint script for the docker image.

This script tries to find the FTP related variables in the environment and
create a ~/.netrc file.
After that it calls the original ``docker-entrypoint`` script from the
base docker image.`

"""

import argparse
import stat
from subprocess import call
import os
import pwd
import shutil

airflow_pwnam = pwd.getpwnam('airflow')
airflow_uid = airflow_pwnam.pw_uid
airflow_gid = airflow_pwnam.pw_gid

def write_netrc_config():
    ftp_host = os.getenv("FTP_PASSIVE_ADDRESS")
    ftp_user = os.getenv("FTP_USER")
    ftp_password = os.getenv("FTP_PASSWORD", ftp_user)
    netrc_path = os.path.expanduser("/home/airflow/.netrc")
    if ftp_host is not None and not os.path.isfile(netrc_path):
        print("Generating /home/airflow/.netrc file...")
        generate_netrc(ftp_host, ftp_user, ftp_password, netrc_path)

def generate_netrc(host, user, password, netrc_path):
    contents = "machine {}\n\tlogin {}\n\tpassword {}\n".format(
        host, user, password)
    with open(netrc_path, "w") as fh:
        fh.write(contents)
    os.chmod(netrc_path, stat.S_IRUSR | stat.S_IWUSR)  # equivalent to chmod 600
    os.chown(netrc_path, airflow_uid, airflow_gid)

def copy_ssh_config():
    """ The container's .ssh path contents must be given in a shared
    volume pointing to the container's path: /tmp/shared_ssh
    """
    shutil.copytree('/tmp/shared_ssh', '/home/airflow/.ssh') 
    os.chown('/home/airflow/.ssh', airflow_uid, airflow_gid)
    for root_path, dirs, files in os.walk('/home/airflow/.ssh'):
        for path in [os.path.join(root_path, dir_name) for dir_name in dirs]:
            os.chown(path, airflow_uid, airflow_gid)
        for path in [os.path.join(root_path, file_name) for file_name in files]:
            os.chown(path, airflow_uid, airflow_gid)

def call_original_entrypoint(entrypoint_args):
    """Run the original entrypoint as 'airflow' user.
    """
    original_entrypoint_command = (
        'exec su airflow -c "docker-entrypoint {}"'.format(' '.join(entrypoint_args))
    )
    print("system call: {}".format(original_entrypoint_command))
    call(original_entrypoint_command, shell=True)

def main(entrypoint_args):
    write_netrc_config()
    copy_ssh_config()
    call_original_entrypoint(entrypoint_args)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    args, remaining_args = parser.parse_known_args()
    main(remaining_args)
