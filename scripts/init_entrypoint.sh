#!/bin/bash

# This is a workaround to call the /sbin/init script 
# as the pid 1 process, after fork the "real" entrypoint
# script it self.

entrypoint(){
    echo About to call: cglops-dissemination-entrypoint $@
    echo in 10 sec...
    sleep 10 
    cglops_dissemination_entrypoint.py $@ 
    echo Exit entrypoint.
}

entrypoint $@ &
exec init
