"""Airflow DAGs for the cglops dissemination processing lines"""

import datetime as dt
from getpass import getuser
import logging
import os

from airflow import DAG
from airflow import configuration
from airflow.operators import SokoOperator
from airflow.operators.sensors import TimeDeltaSensor

logger = logging.getLogger(__name__)
PROJECT_ROOT = os.path.dirname(__file__)


def _apply_logging_config():
    """This is a workaround for a bug in airflow v1.8.0 that does not respect
    its own logging configuration when running the ``airflow test`` cli
    command"""
    log_level = configuration.get("core", "logging_level")
    logging.root.setLevel(log_level.upper())


_apply_logging_config()


def _get_version(project_root):
    """Workaround for being able to import this without having to use airflow

    This is mainly useful for testing. As such we set the ``__version__`` to
    _test_

    """
    try:
        with open("{}/VERSION".format(project_root)) as fh:
            version = fh.read().strip()
    except IOError:
        version = "dev"
    return version


__version__ = _get_version(PROJECT_ROOT)


def create_dissemination_dag(product_name, schedule_interval,
                             delay_hours=0,
                             catalogue_task_sla=3,
                             temporal_slot_context_key="ts",
                             retry_number=9):
    """Create an airflow DAG designed specifically for CGLOPS dissemination.

    CGLOPS dissemination DAGs are created in an opinionated way, with some
    implicit conventions in place:

    * One DAG per disseminated product - This makes it easier to monitor
      DAGRuns in the airflow UI;
    * DAGs are named after the product that they disseminate;
    * DAGs use only the `airflow.operators.python_operator.PythonOperator`
    * Processing tasks make use of the ``soko`` project and all processing
      tasks are configured as soko packages;
    * The ``airflow.cfg`` file has some soko related settings as well
    * tasks are named in such a way as to make it easy to know which soko
      package they are driving. A task's name follows the convention
      ``<soko_package>_<lowercase_product_name>``

    """

    dag = DAG(
        dag_id="disseminate_{}".format(product_name),
        description="v{} - NRT tasks for disseminating "
                    "{} products".format(__version__, product_name.upper()),
        schedule_interval=schedule_interval,
        default_args={
            "owner": getuser(),
            "depends_on_past": False,
            "start_date": dt.datetime(2017, 1, 1, 0, 0),
            "email_on_failure": False,
            "email_on_retry": False,
            "provide_context": True,
            "temporal_slot_context_key": temporal_slot_context_key,
        },
        params={
            "dag_version": __version__,
            "product": product_name
        }
    )
    retrieve_product = SokoOperator(
        soko_package="retrieve_product_{}".format(product_name),
        task_id="retrieve_product",
        dag=dag,
        retries=retry_number,
        retry_delay=dt.timedelta(minutes=5),
    )
    retrieve_product.doc_md = (
        "The {id!r} task tries to find the {product!r} product locally. When "
        "the product is found, the task succeeds, kicking off the remaining "
        "tasks of the DAG.".format(id=retrieve_product.task_id,
                                   product=product_name.upper)
    )

    send_product_to_dissemination_store = SokoOperator(
        soko_package="send_product_to_dissemination_store_{}".format(
            product_name),
        task_id="send_product_to_dissemination_store",
        dag=dag,
        retries=retry_number,
        retry_delay=dt.timedelta(minutes=5),
        soko_execution_kwargs={
            "move_outputs_to_final_destination": False
        }
    )
    send_product_to_dissemination_store.doc_md = (
        "The {id!r} task sends the {product!r} to the dissemination store, "
        "thus making the product accessible to be downloaded by external "
        "users".format(id=send_product_to_dissemination_store.task_id,
                       product=product_name.upper())
    )

    generate_csw_record = SokoOperator(
        soko_package="generate_csw_record_{}".format(product_name),
        task_id="generate_csw_record",
        dag=dag,
        sla=dt.timedelta(hours=catalogue_task_sla)
    )
    generate_csw_record.doc_md = (
        "The {id!r} task generates a new ISO19115 compliant metadata record, "
        "for the {product!r} product and uploads it to the configured OGC CSW "
        "catalogues. The catalogues are configured in the "
        "*context.metadata_catalogues* section of the settings file."
        "users".format(id=generate_csw_record.task_id,
                       product=product_name.upper())
    )

    generate_preview = SokoOperator(
        soko_package="generate_preview_{}".format(product_name),
        task_id="generate_preview",
        dag=dag,
        retries=retry_number,
        retry_delay=dt.timedelta(minutes=5),
        soko_execution_kwargs={
            "move_outputs_to_final_destination": False
        }
    )
    generate_preview.doc_md = (
        "The {id!r} task generates a PNG image with a preview of the "
        "{product!r} product.".format(id=generate_preview.task_id,
                                      product=product_name.upper())
    )

    if "LST" in product_name.upper():
        process_subscriptions = SokoOperator(
            soko_package="process_subscriptions_{}".format(product_name),
            task_id="process_subscriptions",
            dag=dag,
        )
        process_subscriptions.doc_md = (
            "The {id!r} task communicates with the configured dissemination "
            "systems, requesting that any active subscriptions for the "
            "{product!r} product be dispatched. Existing dissemination systems "
            "are configured in the *context.dissemination_systems* section of the"
            "settings file".format(id=process_subscriptions.task_id,
                                   product=product_name.upper())
        )
    if delay_hours > 0:
        delay_execution = TimeDeltaSensor(
            delta=dt.timedelta(hours=delay_hours),
            poke_interval=60*5,  # try every 5 minutes
            task_id="delay_execution",
            dag=dag
        )
        delay_execution.doc_md = (
            "In order to compensate for the delay associated with the "
            "upstream NRT processing lines, this task delays execution of "
            "the remaining tasks by {} hours".format(delay_hours)
        )
        retrieve_product.set_upstream(delay_execution)
    send_product_to_dissemination_store.set_upstream(retrieve_product)
    generate_preview.set_upstream(retrieve_product)
    generate_csw_record.set_upstream(send_product_to_dissemination_store)
    generate_csw_record.set_upstream(generate_preview)
    if product_name.upper() == "LST":
        process_subscriptions.set_upstream(generate_csw_record)
    return dag


# LST10 products' schedule_interval is configured by means of a cron
# expression. More information on this type of expressions is available at:
#
#     https://en.wikipedia.org/wiki/Cron#CRON_expression
#
PRODUCTS = {
    "DSLF": {
        "schedule_interval": "@hourly",
        "catalogue_task_sla": 3,
        "delay_hours": 1,
        "retry_number": 36, # keep trying for the next 3 hours (36x5 min)
    },
    "DSSF": {
        "schedule_interval": "@hourly",
        "catalogue_task_sla": 3,
        "delay_hours": 1,
        "retry_number": 36, # keep trying for the next 3 hours (36x5 min)
    },
    "LST": {
        "schedule_interval": "@hourly",
        "catalogue_task_sla": 3,
        "delay_hours": 1,
        "retry_number": 36, # keep trying for the next 3 hours (36x5 min)
    },
    "LST10_DC": {
        # Run after 23:00h on the 1st, 11th and 21st days of each month
        "schedule_interval": "0 23 1,11,21 * *",
        "catalogue_task_sla": 24,
        "delay_hours": 2,
        "retry_number": 36, # keep trying for the next 3 hours (36x5 min)
    },
    "LST10_TCI": {
        # Run after 0:00h on the 1st, 11th and 21st days of each month
        "schedule_interval": "0 0 1,11,21 * *",
        "catalogue_task_sla": 24,
        "delay_hours": 2,
        "retry_number": 36, # keep trying for the next 3 hours (36x5 min)
    },
}

for product_name, product_parameters in PRODUCTS.items():
    dissemination_dag = create_dissemination_dag(
        product_name.lower(), **product_parameters)
    globals()[dissemination_dag.dag_id] = dissemination_dag

input_cleaning_dag = DAG(
    "cleanup_inputs",
    description="v{} - Clean up old input files".format(__version__),
    default_args={
        "owner": getuser(),
        "depends_on_past": False,
        "start_date": dt.datetime(2017, 1, 1, 0, 0),
        "email_on_failure": False,
        "email_on_retry": False,
        "provide_context": True,
    },
    schedule_interval="@daily",
    params={
        "dag_version": __version__,
    }
)

SokoOperator(
    soko_package="clean_inputs",
    task_id="clean",
    dag=input_cleaning_dag
)


hourly_output_cleanup_dag= DAG(
    "cleanup_hourly_outputs",
    description="v{} - Clean up old output files".format(__version__),
    default_args={
        "owner": getuser(),
        "depends_on_past": False,
        "start_date": dt.datetime(2017, 1, 1, 0, 0),
        "email_on_failure": False,
        "email_on_retry": False,
        "provide_context": True,
    },
    schedule_interval="@hourly",
    params={
        "dag_version": __version__,
    }
)
SokoOperator(
    soko_package="clean_hourly_outputs",
    soko_execution_kwargs={
        "restrict_retrieval_schemes": ["file"],
    },
    task_id="clean",
    dag=hourly_output_cleanup_dag
)

dekadal_output_cleanup_dag= DAG(
    "cleanup_dekadal_outputs",
    description="v{} - Clean up old output files".format(__version__),
    default_args={
        "owner": getuser(),
        "depends_on_past": False,
        "start_date": dt.datetime(2017, 1, 1, 0, 0),
        "email_on_failure": False,
        "email_on_retry": False,
        "provide_context": True,
    },
    schedule_interval="0 0 1,11,21 * *",
    params={
        "dag_version": __version__,
    }
)
SokoOperator(
    soko_package="clean_dekadal_outputs",
    soko_execution_kwargs={
        "restrict_retrieval_schemes": ["file"],
    },
    task_id="clean",
    dag=dekadal_output_cleanup_dag
)
