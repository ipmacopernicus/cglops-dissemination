"""Unit tests for dags/cglopsdissemination.py"""

import datetime as dt
import os
import sys

from faker import Factory
import mock
import pytest

TESTS_ROOT = os.path.dirname(__file__)
PROJECT_ROOT = os.path.dirname(TESTS_ROOT)
sys.path.append(
    os.path.join(PROJECT_ROOT, "dags"))

import dags

fake = Factory.create()


def test_create_dag():
    default_args = {
        fake.word(): fake.word(),
        fake.word(): fake.word(),
        fake.word(): fake.word(),
    }
    product_name = fake.word()
    with mock.patch("dags.DAG",
                    autospec=True) as mock_DAG, \
            mock.patch("dags.SokoOperator",
                       autospec=True) as mock_SokoOperator:
        dags.create_dag(product_name, default_args)
        mock_DAG.assert_called_once_with(
            "cglops_dissemination_{}".format(product_name.lower()),
            schedule_interval=dt.timedelta(minutes=2),
            default_args=default_args,
            params={"product": product_name}
        )
        assert mock_PythonOperator.call_count == 6
